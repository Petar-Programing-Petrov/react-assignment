const apiKey = process.env.REACT_APP_API_KEY

//So we can write in the API 
export const createHeaders = () => {
    return {
        'Content-Type': 'application/json',
        'x-api-key': apiKey
    }

}