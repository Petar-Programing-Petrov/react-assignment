# Translation App

## Description

This project was created for learning purposes!\
Single Page Application using the React framework to translates only the typed-in text to sign language.

### Project component tree

 [Diagram](https://gitlab.com/Petar-Programing-Petrov/react-assignment/-/blob/main/public/react-assignment-diagram.drawio.pdf)

 ## Table of Contents

- [Getting started](#getting-started)
- [Installation](#installation)
- [Commands for our React project](#commands-for-our-React-project)
- [Usage](#usage)
- [Support](#support)
- [Contributing](#contributing)
- [Authors and acknowledgment](#authors-and-acknowledgment)
- [License](#license)
- [Project status](#project-status)



## Getting started

### To see the deployed product please visit :
https://react-project-translator.herokuapp.com/

### To get the app running locally you need to:


    1- clone the repo
    In the project directory, you can run:
    2- "npm install"
    3- "npm start"

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser. 


### Clone the repo


```
- git clone https://gitlab.com/Petar-Programing-Petrov/react-assignment.git
- cd existing_repo
- open new terminal (power shell) in Visual Studio Code and type "npm install" & "npm start"(to run the app locally)
```

## Installation
Make sure you have installed at least the following tools:
```
• A text editor of your choice (Visual Studio Code recommended)
• You will also use your Browser’s Developer Tools for testing and debugging.
• NPM/Node.js(LTS-Long Term Support version)
• Draw.io (https://drawio-app.com/)
• Browser developer tools for react 
Chrome extension for React Developer Tools:
https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi
• GitLab/GitHub
• Heroku (for deployment)
```

## Commands for our React project

1- npx create-react-app "name of project" (to create the react project)

2- npm start (to start local server)

3- "ctrl + c" in power shell to stop the local server

4- npm install react-router-dom

5- npm install react-hook-form (server must be stopped for this one)

6- npm install redux react-redux

7- npm install redux-devtools-extension (optional but recommended)

For more info please visit https://create-react-app.dev/docs/getting-started/


## Usage
First log in with an username (min 3 characters).\
Then you will be redirected to the translation page.\
Type something in the input box (max 40 symbols).\
When clicking on translate button you will see your text translated to the sing language.\
With buttons on top you can navigate between your profile and the translator.\
On profile page you can see your translation history.\
With the button clear translation history you can delete the whole history and with logout you will be redirected to the login page.



## Support
Please contact the authors of the project to get needed support.


## Contributing
This project is open to contributions.
Only requirements are to follow the Git standards for committing and in general working with git.


## Authors and acknowledgment
This project exists thanks to @angelova24 and @Petar-Programing-Petrov!\
Owing to the sample project videos from @sumodevelopment

## License
Open source project

## Project status
The project works perfectly and is completed taking in mind the project requirements.
